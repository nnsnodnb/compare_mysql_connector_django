# compare_mysql_connector_django

## Environments

- Docker 18.09.2
  - docker-compose 1.23.2
  - Python 3.7.0
  - pip==19.2
    - pipenv==2018.11.26
    - django~=2.1.0
    - mysqlclient==1.4.2.post1
    - PyMySQL==0.9.3
  - MariaDB 10.2

## [mysqlclient demo](https://bitbucket.org/nnsnodnb/compare_mysql_connector_django/src/master/mysqlclient_con/)

- [PyPI](https://pypi.org/project/mysqlclient/)

```bash
$ cd mysqlclient_con
$ docker-compose up --build -d
```

## [PyMySQL demo](https://bitbucket.org/nnsnodnb/compare_mysql_connector_django/src/master/pymysql_con/)

- [PyPI](https://pypi.org/project/PyMySQL/)

```bash
$ cd pymysql_con
$ docker-compose up --build -d
```

**Debugging?**

Change directory to each django projects.

```bash
$ docker-compose exec app bash
root@CONTAINER_ID:/# cd /src
root@CONTAINER_ID:/# pipenv shell
```

**Finish?**

```bash
$ docker volume rm mysqlclient_con_db_data pymysql_con_db_data
```
