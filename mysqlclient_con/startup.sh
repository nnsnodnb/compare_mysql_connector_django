#!/bin/bash

cd /src

pipenv install

wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
&& tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
&& rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

dockerize -wait tcp://db:3306 -timeout 60s

pipenv run python manage.py migrate
pipenv run python manage.py runserver 0.0.0.0:8000
