from django.conf import settings
from django.db import models
from django.utils.timezone import now


class Task(models.Model):

    title = models.CharField(
        default='', max_length=200, blank=False, null=False, help_text='タイトル'
    )
    detail = models.CharField(
        default='', max_length=200, blank=False, null=False, help_text='詳細'
    )
    created_at = models.DateTimeField(
        default=now, blank=False, null=False, help_text='作成日'
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE, help_text='所有者'
    )

    def __str__(self):
        return f'{self.title}'

    @classmethod
    def create_tasks_by_loop(cls, count=1000):
        for _ in range(count):
            Task().save()

    @classmethod
    def create_tasks_by_bulk(cls, count=1000):
        Task.objects.bulk_create([Task() for _ in range(count)])

    @classmethod
    def query_tasks(cls, count):
        for _ in range(count):
            tasks = Task.objects.select_related('owner').all()
            assert isinstance(len(tasks), int)

    @classmethod
    def query_tasks_with_length(cls, count, length):
        if count == 1:
            tasks = Task.objects.select_related('owner').all()[:length]
            assert isinstance(len(tasks), int)
            return

        for _ in range(count):
            tasks = Task.objects.select_related('owner').all()[:length]
            assert isinstance(len(tasks), int)

    @classmethod
    def update_tasks_by_loop(cls):
        for task in Task.objects.select_related('owner').all():
            task.title = 'edited'
            task.save()

    @classmethod
    def update_tasks_by_bulk(cls):
        Task.objects.select_related('owner').all().update(title='edited')

    @classmethod
    def delete_tasks_by_loop(cls):
        for task in Task.objects.all():
            task.delete()

    @classmethod
    def delete_tasks_by_bulk(cls):
        Task.objects.all().delete()
